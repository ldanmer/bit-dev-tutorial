# Bit React tutorial

This repository accompanies the Bit React tutorial.
It gives a hands-on demonstration with example code on how to share, use and update a component between two real-world React applications. 

## Overview

Bit lets you share and sync components between different projects and applications.  
In this tutorial, we'll share a React component between two projects.

### What Will You Learn?

In this tutorial you will learn how to:  

- Setup Bit
- Share a React component from an existing project
- Preview the exported component on the Bit cloud
- Install the component in another project
- Modify the React component on the new project
- Get component updates
